function Link() {
    this.hp = 100;
    this.attackDmg = 10;
    this.speed = 50;
    this.fairies = 2;
    this.bombs = 3;
    this.bombDamage = 20;
    this.arrows = 3;
    this.bowDmg = 25;
}

function Ganondorf() {
    this.hp = 200;
    this.attackDmg = 15;
    this.speed = 30;
}

Ganondorf.prototype.attack = function() {
	link.hp -= this.attackDmg;
};

Ganondorf.prototype.warlockPunchDmg = 10;

Ganondorf.prototype.warlockPunch = function() {
	link.hp -= this.warlockPunchDmg;
	link.speed -= this.warlockPunchDmg;
	alert("Link's Speed Drops!");
}

Ganondorf.prototype.randomAttack = function() {
	var randomAtk = Math.random();
	if (randomAtk < 0.49) {
		randomAtk = this.attack();
	} else {
		randomAtk = this.warlockPunch();
	}
	return randomAtk;
}

Link.prototype.attack = function() {
	ganondorf.hp -= this.attackDmg
};

Link.prototype.fairyBottle = function() {
    if (this.fairies > 0) {
        this.hp = 100;
        this.fairies--;
    }
    if (this.fairies == 0) {
        fairyButton.disabled = true;
    }
};

Link.prototype.bomb = function() {
    if (this.bombs > 0) {
        ganondorf.hp -= this.bombDamage;
        this.bombs--;
    }
    if (this.bombs == 0) {
        bombButton.disabled = true;
    }
};

Link.prototype.fairyBow = function() {
	if (this.arrows > 0) {
		ganondorf.hp -= this.bowDmg;
		this.arrows--
	}
	if (this.arrows == 0) {
        arrowButton.disabled = true;
    }
};

var linkHpEl = document.getElementById('link-hp');
var ganonHpEl = document.getElementById('ganon-hp');


var link;
var ganondorf;

function startGame() {
    link = new Link();
    ganondorf = new Ganondorf();
    updateHpDisplay(link, ganondorf);
    attackButton.disabled = false;
    fairyButton.disabled = false;
    bombButton.disabled = false;
    arrowButton.disabled = false;
}

function gameOver() {
    attackButton.disabled = true;
    fairyButton.disabled = true;
    bombButton.disabled = true;
    arrowButton.disabled = true;
    alert('Game Over');
}

function updateHpDisplay() {
    if (link.hp > 0) {
        linkHpEl.innerText = link.hp;
    } else {
        linkHpEl.innerText = 'Dead';
    }

    if (ganondorf.hp > 0) {
        ganonHpEl.innerText = ganondorf.hp;
    } else {
        ganonHpEl.innerText = 'Dead';
    }
}

function round(linkAction, ganonAction) {
    var first, second;
    if (link.speed >= ganondorf.speed) {
        first = linkAction;
        second = ganonAction;
    } else {
        first = ganonAction;
        second = linkAction;
    }

    first();
    updateHpDisplay();
    if (ganondorf.hp <= 0 || link.hp <= 0) {
    	gameOver();
    	return;
    }

    second();
   	updateHpDisplay();
    if (ganondorf.hp <= 0 || link.hp <= 0) {
    	gameOver();
   		return;
    }  
}

function fairyBottle() {
    if (link.fairies > 0) {
        link.hp = 100;
        link.fairies--;
    }
    if (link.fairies == 0) {
        fairyButton.disabled = true;
    }
}

function bomb() {
    if (link.bombs > 0) {
        ganondorf.hp -= link.bombDamage;
        link.bombs--;
    }
    if (link.bombs == 0) {
        bombButton.disabled = true;
    }
}

function fairyBow() {
	if (link.arrows > 0) {
		ganondorf.hp -= link.bowDmg;
		link.arrows--
	}
	if (link.arrows == 0) {
        arrowButton.disabled = true;
    }
}

document.getElementById('new-game').addEventListener('click', function(event) {
    startGame();
});

var attackButton = document.getElementById('attack');
attackButton.addEventListener('click', function(event) {
    round(link.attack.bind(link), ganondorf.randomAttack.bind(ganondorf));
});

var fairyButton = document.getElementById('fairy');
fairyButton.addEventListener('click', function(event) {
    round(link.fairyBottle.bind(link), ganondorf.randomAttack.bind(ganondorf));
});

var bombButton = document.getElementById('bomb');
bombButton.addEventListener('click', function(event) {
    round(link.bomb.bind(link), ganondorf.randomAttack.bind(ganondorf));
});

var arrowButton = document.getElementById('fairybow');
arrowButton.addEventListener('click', function(event) {
    round(link.fairyBow.bind(link), ganondorf.randomAttack.bind(ganondorf));
});